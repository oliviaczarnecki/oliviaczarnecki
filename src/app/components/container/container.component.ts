import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {
  @ViewChild('menuButton', {read: MatMenuTrigger}) protected menuButton : MatMenuTrigger;
  
  @ViewChild('contactMe') private contactRef: ElementRef;
  @ViewChild('aboutMe') private aboutRef: ElementRef;
  @ViewChild('experience') private experienceRef: ElementRef;
  @ViewChild('education') private educationRef: ElementRef;
  @ViewChild('skills') private skillsRef: ElementRef;

  public navRef: string = "contact";
  public email: string = "[hidden]";
  public mobile: string = "[hidden]";
  private hiddenEmail: string = "TGl2dmllLkN6YXJuZWNraUBnbWFpbC5jb20=";
  private hiddenMobile: string = "MjE1LTg1MC03NTIz";
  public hidden:boolean = true;

  constructor() { }

  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true); //third parameter
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

  scroll = (event:MouseEvent): void => {
    this.setNavigation();
  };

  setNavigation() {
    if (this.isScrolledIntoView(this.skillsRef.nativeElement)) this.navRef = "experience";
    else if (this.isScrolledIntoView(this.educationRef.nativeElement)) this.navRef = "experience";
    else if (this.isScrolledIntoView(this.aboutRef.nativeElement)) this.navRef = "about";
    else if (this.isScrolledIntoView(this.experienceRef.nativeElement)) this.navRef = "experience";
    else if (this.isScrolledIntoView(this.contactRef.nativeElement)) this.navRef = "contact";
  }

  private isScrolledIntoView(el) {
    var rect = el.getBoundingClientRect();
    var elemTop = rect.top;
    var elemBottom = rect.bottom;

    var isVisible:boolean;// = (elemTop >= 0) && (elemBottom <= window.innerHeight); //full visibility
    isVisible = elemTop < window.innerHeight && elemBottom >= 0; //partial visibility
    return isVisible;
  }

  navigateTo(elementName: string) {
    this.menuButton.closeMenu();
    switch (elementName) {
      case "contact": {
        this.contactRef.nativeElement.scrollIntoView({ behavior: "smooth" });
        break;
      }
      case "about": {
        this.aboutRef.nativeElement.scrollIntoView({ behavior: "smooth" });
        break;
      }
      case "experience": {
        this.experienceRef.nativeElement.scrollIntoView({ behavior: "smooth" });
        break;
      }
      case "education": {
        this.educationRef.nativeElement.scrollIntoView({ behavior: "smooth" });
        break;
      }
      case "skills": {
        this.skillsRef.nativeElement.scrollIntoView({ behavior: "smooth" });
        break;
      }
    }
  }

  getCI() {
    if(this.hidden) {
      this.email = window.atob(this.hiddenEmail);
      this.mobile = window.atob(this.hiddenMobile);
      this.hidden = false;
    } else {
      this.email = "[hidden]";
      this.mobile = "[hidden]";
      this.hidden = true;
    }
    console.log(this.hiddenEmail,this.hiddenEmail,this.email);
    return false;
  }

}
