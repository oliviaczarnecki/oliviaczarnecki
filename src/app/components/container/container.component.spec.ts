import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { ContainerComponent } from './container.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { LayoutModule } from '@angular/cdk/layout';
import { AppMaterialModule } from 'src/app/modules/material.module';
import { HttpClientModule } from '@angular/common/http';

describe('ContainerComponent', () => {
  let component: ContainerComponent;
  let fixture: ComponentFixture<ContainerComponent>;

  beforeEach((() => {
    TestBed.configureTestingModule({
      declarations: [ContainerComponent],
      imports: [
        BrowserModule,
        LayoutModule,
        HttpClientModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
        AppMaterialModule,
        FlexLayoutModule
      ]
    }).compileComponents();
  }));

  it('should create container compontent', () => {
    const fixture = TestBed.createComponent(ContainerComponent);
    const component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should show contact information', async(() => {
    const fixture = TestBed.createComponent(ContainerComponent);
    const component = fixture.componentInstance;

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      let button = fixture.debugElement.nativeElement.querySelector(".contact button");
      button.click();

      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(component.hidden).toEqual(false);
        expect(component.email).toEqual("Livvie.Czarnecki@gmail.com");
        expect(component.mobile).toEqual("215-850-7523");
      });
    })
  }));
});
