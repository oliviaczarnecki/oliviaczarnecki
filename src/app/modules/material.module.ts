import { NgModule } from '@angular/core';

import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import {MatMenuModule} from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  imports: [
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule
  ],
  exports:[
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule  ],
  providers: []
})
export class AppMaterialModule {
    private path: string = '/assets/icons';
    constructor(private domSanitizer: DomSanitizer, public matIconRegistry: MatIconRegistry) {
      this.matIconRegistry.addSvgIcon('html5', this.setPath(`${this.path}/brands/HTML5.svg`));
      this.matIconRegistry.addSvgIcon('jquery', this.setPath(`${this.path}/brands/jquery.svg`));
      this.matIconRegistry.addSvgIcon('sass', this.setPath(`${this.path}/brands/sass.svg`));
      this.matIconRegistry.addSvgIcon('angular', this.setPath(`${this.path}/brands/angular.svg`));
      this.matIconRegistry.addSvgIcon('aspnet', this.setPath(`${this.path}/brands/aspnet.svg`));
      this.matIconRegistry.addSvgIcon('bootstrap', this.setPath(`${this.path}/brands/bootstrap.svg`));
      this.matIconRegistry.addSvgIcon('matdesign', this.setPath(`${this.path}/brands/material_design.svg`));
      this.matIconRegistry.addSvgIcon('nodejs', this.setPath(`${this.path}/brands/Node.js.svg`));
      this.matIconRegistry.addSvgIcon('java', this.setPath(`${this.path}/brands/java.svg`));
      this.matIconRegistry.addSvgIcon('ibmdb2', this.setPath(`${this.path}/brands/ibm-db2.svg`));
      this.matIconRegistry.addSvgIcon('mongodb', this.setPath(`${this.path}/brands/MongoDB.svg`));
      this.matIconRegistry.addSvgIcon('mysql', this.setPath(`${this.path}/brands/mysql.svg`));
      this.matIconRegistry.addSvgIcon('ecmascript', this.setPath(`${this.path}/brands/ecmascript.svg`));
      this.matIconRegistry.addSvgIcon('css3', this.setPath(`${this.path}/brands/CSS3.svg`));
      this.matIconRegistry.addSvgIcon('expressjs', this.setPath(`${this.path}/brands/expressjs.svg`));
      this.matIconRegistry.addSvgIcon('typescript', this.setPath(`${this.path}/brands/typescript.svg`));  
    }
    private setPath(url: string): SafeResourceUrl {
        return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
    }
 }
